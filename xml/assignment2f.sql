SELECT 
XMLCONCAT(
	XMLELEMENT(NAME "movie",
	XMLELEMENT(NAME "year", m.name),
	XMLELEMENT(NAME "year", m.year),
	XMLELEMENT(NAME "rating", m.rating),
	(SELECT XMLAGG (XMLELEMENT(NAME "actor", a.role) ) FROM acts a where m.mid=a.mid ),
	(SELECT XMLAGG (XMLELEMENT(NAME "director", d.pid) ) FROM directs d where m.mid=d.mid )
	)
	)
FROM movie m;
 


