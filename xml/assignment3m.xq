(for $m in distinct-values(doc("voc.xml")//master)
order by fn:count(//master[text()=$m]) descending
return  <master>{$m}</master>)[0]