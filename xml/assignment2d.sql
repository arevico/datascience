 SELECT 
 XMLELEMENT(
	 NAME "movie", XMLATTRIBUTES(m.mid as "id"),
	 XMLCONCAT(
	 XMLELEMENT(NAME "name",m.name),
 		XMLELEMENT(NAME "year", m.year),
 		XMLELEMENT(NAME "rating", m.rating)) 
	)
 FROM movie m;