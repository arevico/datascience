(for $v in doc("voc.xml")//voyage
	where not ($v//boatname)
	order by ($v//number) ascending
return $v)[1]