SELECT 
XMLCONCAT(
	XMLELEMENT(NAME "movie", 
	
	XMLCONCAT(		
		XMLELEMENT(NAME "movie", m.name),
		XMLELEMENT(NAME "year", m.year),
		XMLELEMENT(NAME "rating", m.rating),
		XMLAGG(	
			XMLELEMENT(NAME "role", a.role)
		)
	)
	))

FROM movie m 
INNER JOIN acts a 
ON (a.mid=m.mid) 
GROUP BY m.name,m.year,m.rating;